
class Board:
    """ Create a Board with columns and rows. Can print the board in a proper way """
    def __init__(self):
        self.title = "Unknown"
        self.column_title = []
        self.column_data = []
        self.number_of_column = 0
        self.column_size = 0
        self.total_size = 0
        self.extra_bottom_line = None

    def change_title(self, title):
        """Change the title of the board"""
        self.title = title

    def increase_number_of_column(self):
        """Increase the number of column by one"""
        self.number_of_column += 1

    def get_number_of_column(self):
        """Return the number of column"""
        return self.number_of_column

    def change_column_size(self, size):
        """Change the column size by the one given"""
        self.column_size = size

    def change_total_size(self, size):
        """Change the total size of the board by the one given"""
        self.total_size = size

    def add_columns_title(self, column_title):
        """Add titles to the columns"""
        for value in column_title:
            self.column_title.append(value)
            self.increase_number_of_column()
            if len(value) > self.column_size:
                self.change_column_size(len(value))

    def add_rows(self, rows):
        """Add rows to the board"""
        if self.get_number_of_column() == 0:
            print("Sorry bro, your board has no column")
            raise ValueError

        for row in rows:
            new_row = []
            if len(row) == self.number_of_column:
                for value in row:
                    if len(str(value)) > self.column_size:
                        self.change_column_size(len(value) + 2)
                    new_row.append(value)

            else:
                number_missing_value = len(row) - self.number_of_column
                print("Sorry bro, you have {} column in your board and you want to add {} value \n"
                      .format(self.number_of_column,
                              len(row)))
                if number_missing_value > 0:
                    if number_missing_value == 1:
                        print("You have {} missing value\n".format(number_missing_value))
                    else:
                        print("You have {} missing values\n".format(number_missing_value))
                else:
                    print("Sorry, you are giving to much value")

                raise ValueError

            new_total_size = self.column_size * self.number_of_column + (self.number_of_column - 1) + 2

            self.column_data.append(new_row)
            if len(self.title) < new_total_size:
                self.change_total_size(new_total_size)
            else:
                self.change_total_size(len(self.title) + 2)

    def get_column_data(self):
        """Return the column data. Those are the rows"""
        return self.column_data

    def get_print_line(self, values):
        """Create a good shaped line to print"""
        line = "║"

        count = 1

        for name in values:
            name = str(name)
            temp_width = self.column_size - len(name)
            half_temp_width = temp_width // 2

            line += " " * half_temp_width + name

            while len(line) < self.column_size * count + count:
                line += " "

            if len(line) < self.total_size - self.column_size:
                line += "|"

            count += 1

        line += "║\n"
        return line

    def add_extra_line_bottom(self, prompt):
        """Add a extra line bellow the board for the print"""
        extra_bottom_line = "║" + " " * (self.total_size // 2 - (len(prompt) // 2) - 1) + prompt

        while len(extra_bottom_line) != self.total_size - 1:
            extra_bottom_line += " "
        extra_bottom_line += "║\n"

        self.extra_bottom_line = extra_bottom_line

    def get_text(self):
        """Print the board with all the data in it"""
        title_block_top = "╔" + "═" * (self.total_size - 2) + "╗" + "\n"

        title_block_mid = "║" + " " * (((self.total_size - len(self.title)) // 2) - 1) + self.title

        while len(title_block_mid) < self.total_size - 1:
            title_block_mid += " "

        title_block_mid += "║\n"

        title_block_bot = "╠" + "═" * (self.total_size - 2) + "╣" + "\n"

        column_title_line = self.get_print_line(self.column_title)

        blank_line = "║" + "¯" * (self.total_size - 2) + "║" + "\n"

        data_lines = ""

        for values in self.column_data:
            data_lines += self.get_print_line(values)

        bottom_line = ""
        if self.extra_bottom_line is not None:
            bottom_line += "╠" + "═" * (self.total_size - 2) + "╣" + "\n"
            bottom_line += self.extra_bottom_line
        bottom_line += "╚" + "═" * (self.total_size - 2) + "╝" + "\n"

        output = "{}{}{}{}{}{}{}".format(title_block_top,
                                         title_block_mid,
                                         title_block_bot,
                                         column_title_line,
                                         blank_line,
                                         data_lines,
                                         bottom_line)
        return output
