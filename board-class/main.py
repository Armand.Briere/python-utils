from board_class import Board

board = Board("Bonjour")

column_titles = ["Year", "Semester", "Class", "Exam", "Mark", "Maximum", "Weight"]

board.add_column_title(column_titles)

rows = [["2018", "Autumn", "INF1010", "INTRA 01", 13, 20, 25],
        ["2018", "Autumn", "INF1010", "INTRA 02", 14, 20, 25],
        ["2018", "Autumn", "INF1010", "QUIZ 01", 18, 20, 25],
        ["2018", "Autumn", "INF1010", "QUIZ 02", 18, 20, 25]]

board.add_rows(rows)

board.print_board()

