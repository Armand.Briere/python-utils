
class Input:
    raw_input = None
    formatted_input = None

    def __init__(self, prompt):
        """Need a prompt for init like normal input()"""
        self.prompt = prompt

    def input_(self):
        """Def input_ is call by the function check"""
        self.raw_input = input(self.prompt)

    def check(self):
        """Check if raw_input is blank, if not set the formatted_input as the raw_input """
        if self.raw_input == "":
            return False
        else:
            self.formatted_input = self.raw_input
            return True

    def read(self):
        """Call def input_(), unless check is true, keep asking for input"""
        self.input_()
        while not self.check():
            self.input_()

        return self.formatted_input


class InputInt(Input):
    """Inheritance from Input Class"""
    def check(self):
        """Check if the input is a Int"""
        if Input.check(self):
            try:
                self.formatted_input = int(self.raw_input)
                return True
            except ValueError:
                pass
        return False


class InputIntP(InputInt):
    """Inheritance from InputInt Class"""
    def check(self):
        """Check if the input is a positive Int"""
        if InputInt.check(self):
            if self.formatted_input > 0:
                return True
        return False


class InputIntCheck(InputInt):
    """Inheritance from InputInt Class"""
    def __init__(self, prompt, check_value):
        """Define a new init with a new self.check_value"""
        super().__init__(prompt)
        self.check_value = check_value


class InputIntEqual(InputIntCheck):
    """Inheritance from InputIntCheck Class"""
    def check(self):
        """Check if the input is a Int equal to the define value"""
        if InputInt.check(self):
            if self.formatted_input == self.check_value:
                return True
        return False


class InputIntLT(InputIntCheck):
    """Inheritance from InputIntCheck Class"""
    def check(self):
        """Check if the input is a Int less than the define value"""
        if InputInt.check(self):
            if self.formatted_input < self.check_value:
                return True
        return False


class InputIntLTE(InputIntCheck):
    """Inheritance from InputInt Class"""
    def check(self):
        """Check if the input is a Int less or equal than the define value"""
        if InputInt.check(self):
            if self.formatted_input <= self.check_value:
                return True
        return False


class InputIntMT(InputIntCheck):
    """Inheritance from InputIntCheck Class"""
    def check(self):
        """Check if the input is a Int greater than the define value"""
        if InputInt.check(self):
            if self.formatted_input > self.check_value:
                return True
        return False


class InputIntMTE(InputIntCheck):
    """Inheritance from InputIntCheck Class"""
    def check(self):
        """Check if the input is a Int greater or equal than the define value"""
        if InputInt.check(self):
            if self.formatted_input >= self.check_value:
                return True
        return False


class InputListInt(InputIntP):
    """Inheritance from InputIntP Class"""
    def __init__(self, prompt, check_list):
        super(InputListInt, self).__init__(prompt)
        self.check_list = check_list
        # Create a list of options that can be chose
        for key, value in enumerate(self.check_list):
            self.prompt += '{}. {}\n'.format(key + 1, value)

    def check(self):
        """Check if the input is a Int that is in the list"""
        if InputIntP.check(self):
            try:
                if self.check_list[int(self.formatted_input) - 1] in self.check_list:
                    return True
            except IndexError:
                pass
        return False

    def read(self):
        """Return the index of the option chosen"""
        self.input_()
        while not self.check():
            self.input_()

        return self.formatted_input - 1


class InputListStr(InputListInt):
    """Inheritance from InputListInt Class"""
    def read(self):
        """Return the str option chosen"""
        self.input_()
        while not self.check():
            self.input_()

        return self.check_list[int(self.formatted_input) - 1]
