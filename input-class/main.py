from input_class import Input, InputInt, InputIntP, InputIntLT, InputIntLTE, InputIntMT, InputIntMTE, InputListInt,\
    InputListStr

test_00 = Input("Input please :\n").read()
test_01 = InputInt("A number please :\n").read()
test_02 = InputIntP("A positive number please:\n").read()
test_03 = InputIntLT("A number less than 10 please :\n", 10).read()
test_04 = InputIntLTE("A number less than 10 or equal please :\n", 10).read()
test_05 = InputIntMT("A number greater than 10 please :\n", 10).read()
test_06 = InputIntMTE("A number greater than 10 or equal please :\n", 10).read()
test_07 = InputListInt("What do you wanna do ?\n", ["Eat", "Drink", "Sleep"]).read()
test_08 = InputListStr("What do you wanna do ?\n", ["Eat", "Drink", "Sleep"]).read()
print("Input : {}\n"
      "Input Int : {}\n"
      "Input Int positive : {}\n"
      "Input Int less than 10 : {}\n"
      "Input Int less than 10 or equal : {}\n"
      "Input Int greater than 10: {}\n"
      "Input Int great than 10 or equal : {}\n"
      "Input List Int, return index option : {}\n"
      "Input List Str, return Str option: {}\n".format(test_00,
                                                       test_01,
                                                       test_02,
                                                       test_03,
                                                       test_04,
                                                       test_05,
                                                       test_06,
                                                       test_07,
                                                       test_08))
