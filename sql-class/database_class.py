import os
import sqlite3


class Database:
    """ READ : If a part of a function name is plural it means that this param need to be a LIST [] """
    """Class function are : Change_name, Get_name, Create_table, Add_columns, Add_values, Update_values,
                            Change_columns_names, Remove_rows_by_values, Get_columns_values, Get_columns_by_key,
                            Already_in"""

    def __init__(self, dir_path):
        """This class has no init values because the constructor is only called at first launch"""
        self.database_name = "Unknown.db"
        self.dir_path = dir_path
        self.database_path = os.path.join(self.dir_path, self.database_name)

    def change_name(self, name):
        """Change the Database name and add the .db extension"""
        if name[-3:] == ".db":
            self.database_name = name
            self.change_path()
        else:
            self.database_name = name + ".db"
            self.change_path()

    def change_path(self):
        """Change the Database path. Only used when the Database name is changed"""
        self.database_path = os.path.join(self.dir_path, self.database_name)

    def get_name(self):
        """Return name of Database"""
        return self.database_name

    def create_table(self, table_name, first_column_name, column_type, default_value=None):
        """Create a table in the Database, need a first column because SQL doesn't support 0 column table"""
        check_data_type(column_type)

        # Set default_value to Null if none is given
        if default_value is None:
            default_value = "Null"

        # All database.execute() have their execute message done before, easy for debug just add a print(execute)
        execute = "CREATE TABLE {} ({} {} DEFAULT {})".format(table_name, first_column_name, column_type, default_value)

        # Execute = CREATE TABLE table_name (Column_1 Column_1_type DEFAULT Default_value_1,
        #                                    Column_2 Column_2_type DEFAULT Default_value_2, ... ,
        #                                    Column_n Column_n_type DEFAULT Default_value_n)"
        database = sqlite3.connect(self.database_path)
        database.execute(execute)

        database.commit()
        database.close()

    def add_columns(self, table_name, columns_name, columns_type, defaults_value=None):
        """Add column to the table"""
        database = sqlite3.connect(self.database_path)

        # Create a list of Null defaults_value if none is given
        if defaults_value is None:
            defaults_value = ["Null"] * len(columns_name)

        # ALTER TABLE have no loop so we can't add multiple columns at the same time
        # Execute = ALTER TABLE table_name ADD COLUMN column_name column_type DEFAULT default_value
        for key, column in enumerate(columns_name):
            execute = "ALTER TABLE {} ADD COLUMN \"{}\" {} DEFAULT {}".format(table_name, column, columns_type[key],
                                                                          defaults_value[key])
            print(execute)
            database.execute(execute)

        database.commit()
        database.close()

        print("\nColumns have been added")

    def add_values(self, table_name, columns_name, values):
        """Add values to multiple column at a time"""
        # Execute is tricky here so it's creation take some lines
        execute = "INSERT INTO {} (".format(table_name)
        for key, column in enumerate(columns_name):
            execute += "{}".format(str(column))
            if key + 1 != len(columns_name):
                execute += ", "
        execute += ") VALUES ("
        for key, value in enumerate(values):
            execute += "\"{}\"".format(str(value))
            if key + 1 != len(values):
                execute += ", "
        execute += ")"

        # Execute = INSERT INTO table_name (Column_1, Column_2, ... , Column_n)
        #           VALUES ("Value_1", "Value_2" ... , "Value_n")
        database = sqlite3.connect(self.database_path)
        database.execute(execute)

        database.commit()
        database.close()

        print("\nValues have been added")

    def update_values(self, table_name, columns_to_modify, new_values, check_columns, check_value):
        """Change multiple values at a time"""
        # Execute is long as well
        execute = "UPDATE {} SET ".format(table_name)
        for key, column in enumerate(columns_to_modify):
            execute += "{} = \"{}\"".format(column, new_values[key])
            if key + 1 != len(columns_to_modify):
                execute += ", "
        execute += " WHERE "
        for key, value in enumerate(check_value):
            execute += "{} = \"{}\"".format(check_columns[key], value)
            if key + 1 != len(check_value):
                execute += " AND "

        # Execute = UPDATE table_name SET Column_to_modify_1 = "New_Value_1",
        #                                 Column_to_modify_2 = "New_Value_2", ... ,
        #                                 Column_to_modify_n = "New_Value_n"
        #                           WHERE Check_column_1 = "Check_Value_1",
        #                             AND Check_column_2 = "Check_Value_2", ... ,
        #                             AND Check_Column_n = "Check_Value_n"

        # To update value, need a cursor
        database = sqlite3.connect(self.database_path)
        cursor = database.cursor()
        cursor.execute(execute)

        cursor.close()
        database.commit()
        database.close()

        print("\nThe value has been updated\n")

    def change_columns_names(self, table_name, old_columns_names, new_columns_names):
        """Change columns name, this process is long ! It does:
        - Rename first table
        - Create a new table with the first table name and new columns name
        - Transfer data from original table to new one
        - Delete the original table """

        database = sqlite3.connect(self.database_path)
        cursor = database.cursor()
        cursor.execute('''PRAGMA table_info({})'''.format(table_name))
        cursor_list = list(cursor)

        # We generate list per column: Old and New and New type
        old_column_list = []
        new_column_list = []
        new_column_type_list = []
        j = 0
        for key, row in enumerate(cursor_list):
            old_column_list.append(row[1])
            i = 0
            modified = False
            while i < len(old_columns_names):
                if row[1] == old_columns_names[i]:
                    new_column_list.append(new_columns_names[j])
                    j += 1
                    modified = True
                    break
                i += 1
            if not modified:
                new_column_list.append(row[1])
            new_column_type_list.append(row[2])

        execute = "ALTER TABLE {} RENAME TO tmp_table_name".format(table_name)

        # Execute = ALTER TABLE table_name RENAME TO New_table_name
        cursor.execute(execute)

        execute = "CREATE TABLE {} (".format(table_name)
        for key, column in enumerate(new_column_list):
            execute += "{} {}".format(column, new_column_type_list[key])
            if key + 1 != len(new_column_list):
                execute += ", "
        execute += ")"

        # Execute = CREATE TABLE table_name (Column_1 Column_1_type,
        #                                    Column_2 Column_2_type, ... ,
        #                                    Column_n Column_n_type)"
        database.execute(execute)

        execute = "INSERT INTO {} (".format(table_name)
        for key, column in enumerate(new_column_list):
            execute += column
            if key + 1 != len(new_column_list):
                execute += ", "
        execute += ") SELECT "
        for key, column in enumerate(old_column_list):
            execute += column
            if key + 1 != len(old_column_list):
                execute += ", "
        execute += " FROM tmp_table_name"

        # Execute = INSERT INTO table_name (New_Column_1, New_Column_2, ... , New_Column_n)
        #                            Select Old_Column_1, Old_Column_2, ..., Old_Column_n
        #                            FROM old_table_name
        database.execute(execute)

        execute = "DROP TABLE tmp_table_name"

        # Execute = DROP TABLE old_table_name
        database.execute(execute)

        database.commit()
        cursor.close()
        database.close()

    def remove_rows_by_values(self, table_name, columns_name, values):
        """Delete all row with the value in it"""
        execute = "DELETE FROM {} WHERE ".format(table_name)
        for key, column in enumerate(columns_name):
            execute += "{} = \"{}\"".format(column, str(values[key]))
            if key + 1 != len(columns_name):
                execute += " AND "

        # Execute = DELETE FROM table_name WHERE Column_1 = "Value_1"
        #                                    AND Column_2 = "Value_2" AND ...
        #                                    AND Column_n = "Value_n"
        database = sqlite3.connect(self.database_path)
        database.execute(execute)

        database.commit()
        database.close()

    def get_columns_values(self, table_name, columns_name):
        """Return all rows of the selected columns"""
        execute = "SELECT "
        for key, name in enumerate(columns_name):
            execute += name
            if key + 1 != len(columns_name):
                execute += ", "
        execute += " FROM {}".format(table_name)

        # Execute = SELECT Column_1, Column_2 , ... , Column_n FROM table_name
        database = sqlite3.connect(self.database_path)
        cursor = database.cursor()
        cursor.execute(execute)

        values = []
        for row in cursor:
            values.append(row)

        cursor.close()
        database.close()

        return values

    def get_columns_by_key(self, table_name, columns_name, key_columns_name, key_words):
        """Return all row with the matching value in them"""

        execute = "SELECT "
        for key, column in enumerate(columns_name):
            execute += column
            if key + 1 != len(columns_name):
                execute += ", "
        execute += " FROM {} WHERE ".format(table_name)
        for key, column in enumerate(key_columns_name):
            execute += "{} = \"{}\"".format(column, key_words[key])
            if key + 1 != len(key_columns_name):
                execute += " AND "

        # Execute = SELECT Column_1, Column_2, ... , Column_n FROM table_name WHERE Column_1 = "Value_1"
        #                                                                       AND Column_2 = "Value_2" AND ...
        #                                                                       AND Column_n = "Value_n"
        database = sqlite3.connect(self.database_path)
        cursor = database.cursor()
        cursor.execute(execute)

        rows = []
        for row in cursor:
            rows.append(row)

        cursor.close()
        database.close()

        return rows

    def already_in(self, table_name, column_name, value):
        """Boolean, Check if a value is already stored in the Database"""
        execute = "SELECT {} FROM {}".format(column_name, table_name)

        # Execute = SELECT Column_name FROM table_name
        database = sqlite3.connect(self.database_path)
        cursor = database.cursor()
        cursor.execute(execute)

        for row in cursor:
            if row[0].lower() == value.lower:
                cursor.close()
                database.close()
                return True

        cursor.close()
        database.close()
        return False

    def get_length_of_column(self, column_name, table_name):
        execute = "SELECT {} FROM {}".format(column_name, table_name)

        database = sqlite3.connect(self.database_path)
        cursor = database.cursor()
        cursor.execute(execute)

        cursor.close()
        database.close()
        return len(list(cursor))

    def transfer_data(self, initial_table_name, destination_table_name, columns_name, key_columns_name, key_words):
        values = self.get_columns_by_key(initial_table_name, columns_name, key_columns_name, key_words)
        for value in values:
            self.add_values(destination_table_name, columns_name, value)
            self.remove_rows_by_values(initial_table_name, columns_name, value)


def check_data_type(data):
    """Check if the type match one of the possible values for the column type"""
    if data == "NULL" \
            or data == "INTEGER" \
            or data == "REAL" \
            or data == "TEXT" \
            or data == "BLOB":
        pass
    else:
        raise ValueError("Column_type must be : Null, INTEGER, REAL, TEXT or BLOB\n"
                         "Check https://www.sqlite.org/datatype3.html for more info")
