import os
from database_class import Database

PROGRAM_PATH = os.path.join(os.environ['USERPROFILE'], "SQL_TEST")

db = Database(PROGRAM_PATH)

new_name = "Example"

db.change_name(new_name)

name = db.get_name()
print(name)

first_table_name = "First_table"
second_table_name = "Second_table"

first_column = "First_column"
first_column_type = "INTEGER"
default_value = 144

db.create_table(first_table_name, first_column, first_column_type, default_value)
db.add_columns(first_table_name, ["Second_column", "Third_column"], ["REAL", "TEXT"])
db.add_columns(first_table_name, ["SHOW_ME_DAT_DEFAULT_VALUE"], ["REAL"], [158])
db.add_columns(first_table_name, ["NOPE"], ["REAL"], ["Null"])
db.add_values(first_table_name, ["Second_column", "Third_column"], [2, "Three"])
db.add_values(first_table_name, ["First_column", "Second_column", "Third_column"], ["Four", 5, "Six"])
db.add_values(first_table_name, ["Second_column"], [55])

example = db.get_columns_values(first_table_name, ["First_column", "Second_column", "Third_column"])
print(example)

example = db.get_columns_by_key(first_table_name,
                                ["First_column", "Second_column", "Third_column"],
                                ["First_column"],
                                "One")
print(example)

db.remove_rows_by_values(first_table_name, ["Second_column"], [5])
